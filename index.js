const apiUrl = 'http://todos.loc/api/'

$('#notDoneTasks').html('task not done')

var allTasks = []

function fetchTasks () {
  let headers = new Headers({
    'Access-Control-Allow-Origin': '*'
  })

  let tasks
  fetch(apiUrl + 'tasks', { method: 'GET', mode: 'no-cors', headers: headers }).then(response => {
    response.json().then((json) => {
      allTasks = json.tasks.sort((t1, t2) => new Date(t1.deadline).getTime() - new Date(t2.deadline).getTime())
      
      tasks = allTasks.map(t => {
        let deadline = new Date(t.deadline).toLocaleDateString('fr-FR', {})
        return { name: t.name, deadline, _id: t._id, done: t.done }
      })

      let tasksDone = tasks.filter(t => t.done)
      let tasksNotDone = tasks.filter(t => !t.done)
      
      let template = '<div class="list is-hoverable">' +
        '<ul>{{#tasks}}' + 
        '<li class="list-item">nom: {{name}} échéance: {{deadline}}</li>{{/tasks}}</ul></div>'

      let del_icon_template = '<a class="button is-small">' +
      '<span class="icon is-small"><i class="fas fa-trash-alt"></i></span></a>'
      
      let edit_icon_template = '<a class="button is-small">' +
      '<span class="icon is-small"><i class="fas fa-edit"></i></span></a>'

      template = '<table class="table is-fullwidth is-hoverable">'+
      '<thead><th>Nom</th><th>Echeance</th><th>Editer</th><th>Supprimer</th></thead> {{#tasks}} ' + 
      '<tr id={{_id}}><td width=\"60%\">{{name}}</td> <td width="20%">{{deadline}}</td>' + 
      '<td width="10%" onclick="showTaskEditor(\'{{_id}}\')">' + edit_icon_template + '</td><td width="10%" onclick="deleteTask(\'{{_id}}\')">' + del_icon_template + '</td></tr>' +
      '{{/tasks}}</table>'
      $('#notDoneTasks').html(Mustache.to_html(template, { tasks: tasksNotDone }))

      $('#tasksDone').html(Mustache.to_html(template, { tasks: tasksDone }))
    })
  })
    .catch(error => {
      tasks = error.message
      $('#notDoneTasks').html(tasks)
    })
}

$('#addButton').on('click', function() {
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
  let name = $('#newTaskName').val()
  let deadline = new Date($('#newTaskDeadLine').val())
  let done = $('#newTaskStatus').is(':checked')

  let body = JSON.stringify({ name, deadline, done })
  
  fetch(apiUrl + 'add', { method: 'POST', headers, body }).then(response => {
    response.json().then(json => {
      if (json.success) {
        toastSuccess(json.message)
        cleanAddInputs()
      } else {
        toastError('Tâche non ajoutée: ' + json.message)
      }
    })
  })
  .catch(error => {
    toastError(error.message)
  })
  fetchTasks()
})

function cleanAddInputs() {
  $('#newTaskName').val('')
  $('#newTaskDeadLine').val(new Date().toISOString().split('T')[0])
  $('#newTaskStatus').prop('checked', false)
}
function toastError(message) {
  $('#notification').removeClass('is-success').removeClass('is-invisible').addClass('is-danger')
  $('#notification > p').text(message)
}

function toastSuccess(message) {
  $('#notification').removeClass('is-danger').removeClass('is-invisible').addClass('is-success')
  $('#notification > p').text(message)
}

$('#notificationDelete').on('click', function() {
  $('#notification > p').text('')
  $('#notification').addClass('is-invisible')
})

function deleteTask(id) {
  fetch(apiUrl + 'delete/' + id, { method: 'DELETE' }).then(response => {
    response.json().then(json => {
      if (json.success) {
        toastSuccess(json.message)
      } else {
        toastError('Tâche non supprimée: ' + json.message)
      }
    })
  })
  .catch(error => {
    toastError(error.message)
  })
  fetchTasks()
}

function showTaskEditor(id) {
  $('#editor').addClass('is-active')
  $('#editTaskId').val(id)
  let task = allTasks.find(t => t._id === id)
  $('#editTaskName').val(task.name)
  $('#editTaskDeadLine').val(new Date(task.deadline).toISOString().split('T')[0])
  $('#editTaskDone').prop('checked', task.done)
}

function hideTaskEditor() {
  $('#editor').removeClass('is-active')
}

$('#editorClose').on('click', function() {
  hideTaskEditor()
})

function editTask() {
  hideTaskEditor()
  let id = $('#editTaskId').val()
  let task = allTasks.find(t => t._id === id)
  let name = $('#editTaskName').val()
  let deadline = $('#editTaskDeadLine').val()
  let done = $('#editTaskDone').is(':checked')
  task.name = name
  task.deadline = deadline
  task.done = done
  let body = JSON.stringify(task)
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }

  fetch(apiUrl + 'edit', { method: 'POST', headers, body }).then(response => {
    response.json().then(json => {
      if (json.success) {
        toastSuccess(json.message)
        fetchTasks()
      } else {
        toastError('Tâche non éditée: ' + json.message)
      }
    })
  })
  .catch(error => {
    toastError(error.message)
  })
}

$('#editButton').on('click', function() {
  editTask()
})
cleanAddInputs()
fetchTasks()
