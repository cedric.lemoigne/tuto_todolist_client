FROM nginx:1.13.12-alpine as production-stage
COPY . /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/nginx.conf
CMD ["nginx", "-g", "daemon off;"]

EXPOSE 80
